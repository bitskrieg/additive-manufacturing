;; Author - Chris Apsey @bitskrieg
;;
;; This file provides startup gcode for duet/rrf3.
;; The unique feature it provides is that it sets individual per-heater
;; temperatures for tools with multiple heaters, and then waits for them to
;; reach temperature before printing

;; This file will be parsed a little strangely as written because curly
;; brackets are the default method for writing expressions in a few slicers
;; and rrf3, and there is no way to escape them in slicers, for now.  
;; Slicers will complain about unrecognized expressions when slicing,
;; that is OK.

;; IMPORTANT: the !!white_space!! (minus the "_") keyword is used to deal 
;; with slicers that do not respect leading whitespace in their custom gcode
;; sections (such as ideamaker).  You should have a post process script that
;; removes that word, but leaves everything else that comes after it.
;; In Ideamaker, the post process command, minus the "_" would be:
;; {"source":["!!white_space!!"], "type":"remove"}

;; TODO: More elegant solution for whitespace problem, automatic temperature curves

;; pull in relevant slicing settings

;uncomment these lines if you're using ideamaker
var t0_temp={temperature_extruder1} 
var t1_temp={temperature_extruder2}
var bed_temp={temperature_heatbed}

;uncomment these lines if you're using S3D
;var t0_temp=[extruder0_temperature]
;var t1_temp=[extruder1_temperature]
;var bed_temp=[bed0_temperature]

;uncomment these lines if you're using cura
;var t0_temp={material_print_temperature_layer_0, 0}
;var t1_temp={material_print_temperature_layer_0, 1}
;var bed_temp={material_bed_temperature_layer_0}

;uncomment these lines if you're using prusaslicer
;var t0_temp={first_layer_temperature[0]}
;var t1_temp={first_layer_temperature[1]}
;var bed_temp={first_layer_bed_temperature[0]}

;uncomment these lines if you're using slic3r
;var t0_temp=[first_layer_temperature_0]
;var t1_temp=[first_layer_temperature_1]
;var bed_temp=[first_layer_bed_temperature]

; set T0 temperature offsets / set t0 active temps and turn on
; these can be set to whatever makes sense for your polymer/tool
; https://docs.duet3d.com/User_manual/Reference/Gcodes/M568
; https://docs.duet3d.com/en/User_manual/Reference/Gcode_meta_commands
if var.t0_temp != 0
!!whitespace!!  var t0_top_offset=10
!!whitespace!!  var t0_middle_offset=5
!!whitespace!!  var t0_bottom_offset=0
!!whitespace!!  M568 P0 S{var.t0_temp - var.t0_top_offset, var.t0_temp - var.t0_middle_offset, var.t0_temp - var.t0_bottom_offset}A2 

; set T1 temperature offsets / set t1 active temps and turn on
; these can be set to whatever makes sense for your polymer/tool
; https://docs.duet3d.com/User_manual/Reference/Gcodes/M568
; https://docs.duet3d.com/en/User_manual/Reference/Gcode_meta_commands
if var.t1_temp != 0
!!whitespace!!  var t1_top_offset=10
!!whitespace!!  var t1_bottom_offset=0
!!whitespace!!  M568 P1 S{var.t1_temp - var.t1_top_offset, var.t1_temp - var.t1_bottom_offset}A2 

; set heatbed temp
; https://docs.duet3d.com/User_manual/Reference/Gcodes/M140
M140 S{var.bed_temp}

G21 ; use mm
G90 ; set absolute positioning
M82 ; set absolute extrusion
M106 S0 ; turn off fans
M561 ; clear any bed transform
while iterations < #move.axes ; while current iteration (zero-indexed) is less than number of axes (1-indexed)
!!whitespace!!  if !move.axes[iterations].homed ; check if axis is not homed
!!whitespace!!    G28 ; home all axes if above conditional passes
!!whitespace!!    break; exit loop
!!whitespace!!  else
!!whitespace!!    continue; increment and re-evaluate
G32 ;  check bed tilt
G29 S1 ; load mesh map
G92 E0 ; Reset material counter to 0
T0 ; Ensure that tool is selected
M116 S5 ; wait for all set temperatures to be within 5 degrees of targets https://docs.duet3d.com/User_manual/Reference/Gcodes/M116
M117 "Printing..."